
import Foundation

class UserActivityConfig {
    
    struct Api {
        
        #if DEBUG
        static let url = "https://stage.karada.live/api/v1"
        #else
        static let url = "https://karada.live/api/v1"
        #endif
    }
    
    static func dateStr(date: Date) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = TimeZone(identifier: "Asia/Tokyo")
        formatter.locale = Locale(identifier: "ja_JP")
        return formatter.string(from: date)
    }
}
