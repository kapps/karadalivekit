#  UserActivityUploadService
## features

~~~swift
class UserActivityDataUploadService {

    // 当日のHealthkit歩数変更の検知を開始する。
    // Appが終了されない限り、検知後にはKaradaLiveのトークンを更新し、アップロードが実行される。
    // AppがSuspended, Backgroundの状態でも実行される。
    public func startEnableBackgroundDelivery()

    // 実行日時から特定日数遡ってHealthkit歩数をまとめてuploadする。
    // アップロードはRealmの歩数スナップショットがなければ対象として追加し、
    // 追加後は新規にRealmファイルを作成する。
    // Realmの日ごとの歩数スナップショットとHealthkit歩数の比較を行い、
    // 数が異なればアップロード対象に追加される。
    public func uploadBundleStepsDataIfNeeded()
}
~~~

## Setup
###  required

~~~ruby
    pod 'Moya', '~> 11.0'
    pod 'RealmSwift'
~~~

~~~swift
class UserActivityDataUploadService {
    // 起動日時から遡るHealthkitの日数
    public var countPastDaysForChecking
    // KaradaLiveのApiKey
    // トークン更新毎に設定して下さい
    public var karadaLiveApiKey
}

protocol UserActivityDataUploadServiceDelegate {
    // アプリごとのトークンリフレッシュロジックを用いて、成否を返して下さい
    func loginProcedure(completion: @escaping(_ success: Bool) -> Void)
}
~~~

### option

~~~swift
class UserActivityDataUploadService {

    // uploadBundleStepsDataIfNeeded()の最古日を設定出来ます
    public var oldestCheckingDate: Date?
}
~~~
