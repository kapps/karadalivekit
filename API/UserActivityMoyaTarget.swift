
import Foundation
import Moya

public enum UserActivityMoyaTarget {

    case stepAdd(fileUrl: URL, apiKey: String?)
    case stepAddBundle(fileUrl: URL, apiKey: String?)
}

extension UserActivityMoyaTarget: TargetType {
    
    public var baseURL: URL {
        return URL(string: UserActivityConfig.Api.url)!
    }
    
    public var path: String {
        
        switch self {
        case .stepAdd, .stepAddBundle:
            return "/user/step"
        }
    }
    
    public var method: Moya.Method {
        
        switch self {
        case .stepAdd, .stepAddBundle:
            return .post
        }
    }
    
    public var task: Task {
        
        switch self {
        case let .stepAdd(fileUrl, _),
             let .stepAddBundle(fileUrl, _):
            return .uploadFile(fileUrl)
        }
    }
    
    public var sampleData: Data {
        
        switch self {
        case .stepAdd, .stepAddBundle:
            return Data()
        }
    }
    
    public var headers: [String : String]? {
        
        switch self {
        case let .stepAdd(_, apiKey),
             let .stepAddBundle(_, apiKey):
            
            guard let _apiKey = apiKey else {
                fatalError("I need APIKEY")
            }
            
            return [
                "Authorization": "Bearer " + _apiKey,
                "Content-Type": "application/json"
            ]
        }
    }
}
