
import Foundation
import RealmSwift

class UserStep: Object {
    
    @objc dynamic var dateStr: String?
    @objc dynamic var steps: Int = 0
    
    override static func primaryKey() -> String? {
        return "dateStr"
    }
}
