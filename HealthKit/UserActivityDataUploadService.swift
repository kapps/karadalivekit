
import Foundation
import Moya
import HealthKit

protocol UserActivityDataUploadServiceDelegate {
    /// アプリごとのApiリフレッシュロジックを用いて、成否を返して下さい
    func loginProcedure(completion: @escaping(_ success: Bool) -> Void)
}

class UserActivityDataUploadService: NSObject {
    
    static let shared = UserActivityDataUploadService()
    private let stepsDirectory: String = "Steps"
    private var lastUploadedStepsCount: Int = 0
    private var isFirstUploadingBundleData = true
    private var bundleStepData: [[String: Any]] = []
    
    public var karadaLiveApiKey: String?
    public var countPastDaysForChecking: Int?
    public var oldestCheckingDate: Date?
    public var delegate: UserActivityDataUploadServiceDelegate?
    
    public enum Events {
        case uploadSucceeded
    }
    
    private override init() {
        super.init()
    }
    
    public func startEnableBackgroundDelivery() {
        
        let targetType = HKObjectType.quantityType(forIdentifier: .stepCount)!
        let query = HKObserverQuery(sampleType: targetType, predicate: nil, updateHandler: completionHandler)
        HKHealthStore().execute(query)
        HKHealthStore().enableBackgroundDelivery(for: targetType, frequency: .immediate) { success, _ in
            print("# \(#function): \(success)")
        }
    }
    
    // Backgroundで実行する為にjsonを使っている
    public func completionHandler(query: HKObserverQuery, completion: HKObserverQueryCompletionHandler, _: Error?) {
        guard
            let objectType = query.objectType,
            objectType.identifier == HKObjectType.quantityType(forIdentifier: .stepCount)!.identifier
            else {
                return
        }
        let executedDate = Date()
        
        UserActivityService.getActivity(collectionIn: executedDate) { newSteps, newDistance in
            guard
                newSteps != 0,
                newSteps != self.lastUploadedStepsCount
                else {
                    return
            }
            self.lastUploadedStepsCount = newSteps
            print("# UserActivityDataUploadService, detected HealthkitStepsUpdated in Background: newSteps(\(newSteps))")
            
            self.createJson(newSteps: newSteps, newDistance: newDistance, date: executedDate) { success in
                if success == false { return }
                
                guard let delegate = self.delegate else {
                    fatalError("Please implement delegate")
                }
                delegate.loginProcedure { success in
                    if success == false {
                        print(#function, "ApiKey refresh seems to failed.")
                        return
                    }
                    self.uploadStepsInBackgroundSession(date: executedDate)
                    self.uploadBundleDataIfNeeded()
                }
            }
        }
        
    }
    
    public func uploadBundleDataIfNeeded() {
        
        guard let checkingDays = countPastDaysForChecking else {
            fatalError("Must set to dayCounts for checking")
        }
        
        let calendar = Calendar.current
        let oldestDay = calendar.date(byAdding: .day, value: -(checkingDays), to: Date())!
        let daysComponents = calendar.dateComponents([.day], from: oldestDay, to: Date())
        
        self.bundleStepData.removeAll()
        
        let dispatchGroup = DispatchGroup()
        let dispatchQueue = DispatchQueue(label: "\(Bundle.main.bundleIdentifier!).\(#function)")
        
        for value in 1...daysComponents.day! {
            dispatchGroup.enter()
            dispatchQueue.async(group: dispatchGroup) {
                
                if self.shouldAvoidCheckingDate(byAddingDateValue: value) {
                    return dispatchGroup.leave()
                }
                
                guard let checkingDate = calendar.date(byAdding: .day, value: -(value), to: Date()) else {
                    return dispatchGroup.leave()
                }
                
                UserActivityService.getActivity(collectionIn: checkingDate) { healthkitSteps, healthKitDistance in
                    
                    if self.isFirstUploadingBundleData == false,
                        let userStep = UserActivityRealmService.userStep(in: checkingDate) {
                        
                        if userStep.steps == healthkitSteps {
                            return dispatchGroup.leave()
                        }
                    }
                    
                    UserActivityRealmService.userStep(create: healthkitSteps, in: checkingDate) {
                        let dateStr = UserActivityConfig.dateStr(date: checkingDate)
                        self.bundleStepData.append([
                            "date": dateStr,
                            "step": healthkitSteps,
                            "distance": healthKitDistance
                        ])
                        dispatchGroup.leave()
                    }
                }
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            if self.bundleStepData.count == 0 {
                print(#function, "No data to be sent")
                return
            }
            
            self.createBundleJson { success in
                if success == false { return }
                
                self.uploadBundleData()
            }
        }
    }
    
    private func shouldAvoidCheckingDate(byAddingDateValue: Int) -> Bool {
        
        guard let oldestDate = oldestCheckingDate else { return false }
        
        let calendar = Calendar.current
        let oldestDateStart = calendar.startOfDay(for: oldestDate)
        let todayStart = calendar.startOfDay(for: Date())
        
        guard let checkingDate = calendar.date(byAdding: .day, value: -(byAddingDateValue), to: todayStart) else { return true }
        
        let checkingDateStart = calendar.startOfDay(for: checkingDate)
        
        return checkingDateStart < oldestDateStart
    }
    
    public func add(observer: Any, selector: Selector, event: Events) {
        
        let name: Notification.Name?
        
        switch event {
        case .uploadSucceeded:
            name = Notification.Name.UserActivityUploadService.uploadSucceeded
        }
        
        NotificationCenter.default.addObserver(observer,
                                               selector: selector,
                                               name: name,
                                               object: nil)
    }
    
}

// MARK: Step JSON Manager

extension UserActivityDataUploadService {
    
    private func getStepsDirectory() -> URL? {
        
        guard let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return nil
        }
        let stepsDirectoryUrl = path.appendingPathComponent(stepsDirectory)
        
        if FileManager.default.fileExists(atPath: stepsDirectoryUrl.path) == false {
            do {
                try FileManager.default.createDirectory(at: stepsDirectoryUrl, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error)
                return nil
            }
        }
        
        return path
    }
    
    private func createJson(newSteps: Int, newDistance: Double, date: Date, success: @escaping (Bool) -> Void) {
        
        guard let path = getStepsDirectory() else {
            success(false)
            return
        }
        
        let dateStr = UserActivityConfig.dateStr(date: date)
        let fileUrl = path.appendingPathComponent("\(stepsDirectory)/\(dateStr).json")
        do {
            let parameters: [String: Any] = [
                "date": dateStr,
                "step": newSteps,
                "distance": newDistance
            ]
            let jsonData = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions.prettyPrinted)
            try jsonData.write(to: fileUrl)
            
            success(true)
        } catch {
            print(error)
            success(false)
        }
    }
    
    private func createBundleJson(success: @escaping (Bool) -> Void) {
        
        guard let path = getStepsDirectory() else {
            success(false)
            return
        }
        
        let fileUrl = path.appendingPathComponent("\(stepsDirectory)/bundle.json")
        do {
            let parameters: [String: Any] = [
                "list": self.bundleStepData
            ]
            let jsonData = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions.prettyPrinted)
            try jsonData.write(to: fileUrl)
            
            success(true)
        } catch {
            print(error)
            success(false)
        }
    }
    
}

// MARK: Moya

extension UserActivityDataUploadService {
    
    private func uploadStepsInBackgroundSession(date: Date) {
        
        guard let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let url = path.appendingPathComponent("\(stepsDirectory)/\(UserActivityConfig.dateStr(date: date)).json")
        
        print("# \(#function): Began")
        
        MoyaProvider<UserActivityMoyaTarget>().request(.stepAdd(fileUrl: url, apiKey: karadaLiveApiKey)) { result in
            switch result{
            case let .success(moyaResponse):
                do {
                    let _ = try moyaResponse.filterSuccessfulStatusCodes()
                    print("# \(#function): success")
                }
                catch let error {
                    print("# \(#function): failure", error)
                }
            case let .failure(error):
                print("# \(#function): failure", error)
            }
        }
    }
    
    private func uploadBundleData() {
        
        guard let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let url = path.appendingPathComponent("\(stepsDirectory)/bundle.json")
        
        print("# \(#function): Began")
        if self.isFirstUploadingBundleData { self.isFirstUploadingBundleData = false }
        
        MoyaProvider<UserActivityMoyaTarget>().request(.stepAddBundle(fileUrl: url, apiKey: karadaLiveApiKey)) { result in
            switch result{
            case let .success(moyaResponse):
                do {
                    let _ = try moyaResponse.filterSuccessfulStatusCodes()
                    print("# \(#function): success")
                    
                    NotificationCenter.default.post(name: NSNotification.Name.UserActivityUploadService.uploadSucceeded, object: nil)
                }
                catch let error {
                    print("# \(#function): failure", error)
                }
            case let .failure(error):
                print("# \(#function): failure", error)
            }
        }
    }
    
}
