
import HealthKit

extension HKQuantityType {
    
    static var stepCount: HKQuantityType {
        return HKObjectType.quantityType(forIdentifier: .stepCount)!
    }
    static var distanceWalkingRunning: HKQuantityType {
        return HKObjectType.quantityType(forIdentifier: .distanceWalkingRunning)!
    }
}

struct UserActivityService {
    
    static func getStepsUpdatedDate(completion: @escaping (_ steps: Int, _ date: Date?) -> Void) {
    
        let startOfDay = Calendar.current.startOfDay(for: Date())
        let endOfDay = Date()
        
        let predicate = HKQuery.predicateForSamples(withStart: startOfDay, end: endOfDay, options: .strictEndDate)

        let query = HKStatisticsQuery(quantityType: HKObjectType.quantityType(forIdentifier: .stepCount)!, quantitySamplePredicate: predicate, options: .cumulativeSum) { query, result, _ in
            DispatchQueue.main.async {
                guard let sum = result?.sumQuantity() else {
                    completion(0, nil)
                    return
                }
                let count = Int(sum.doubleValue(for: HKUnit.count()))
                completion(count, result?.endDate)
            }
        }

        HKHealthStore().execute(query)
    }
    
    static func getSteps(collectionIn startDate: Date,  completion: @escaping (_ steps: Int) -> Void) {
     
        let startOfDay = Calendar.current.startOfDay(for: startDate)
        let endOfDay = startOfDay.addingTimeInterval((60*60*24)-1)
        
        var dateComponent = DateComponents()
        dateComponent.day = 1
        
        let collectionQuery = HKStatisticsCollectionQuery(
            quantityType: HKObjectType.quantityType(forIdentifier: .stepCount)!,
            quantitySamplePredicate:  HKQuery.predicateForSamples(withStart: startOfDay, end: endOfDay, options: HKQueryOptions()),
            options: [HKStatisticsOptions.separateBySource, HKStatisticsOptions.cumulativeSum],
            anchorDate: startOfDay,
            intervalComponents: dateComponent
        )
        
        collectionQuery.initialResultsHandler = { query, result, error in
            
            guard let result = result, error == nil else {
                return completion(0)
            }
            DispatchQueue.main.async {
                let sorted = result.statistics().sorted(by: {
                    $0.sumQuantity()!.doubleValue(for: HKUnit.count()) > $1.sumQuantity()!.doubleValue(for: HKUnit.count())
                })
                
                guard let first = sorted.first else { return completion(0) }
                
                let steps = Int(first.sumQuantity()!.doubleValue(for: HKUnit.count()))
                completion(steps)
            }
        }
        
        HKHealthStore().execute(collectionQuery)
    }
    
    static func getSteps(startDate: Date, endDate: Date, completion: @escaping (_ steps: Int) -> Void) {
        
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: .strictEndDate)
        
        let query = HKStatisticsQuery(quantityType: HKObjectType.quantityType(forIdentifier: .stepCount)!, quantitySamplePredicate: predicate, options: .cumulativeSum) { query, result, _ in
            DispatchQueue.main.async {
                guard let sum = result?.sumQuantity() else {
                    completion(0)
                    return
                }
                let count = Int(sum.doubleValue(for: HKUnit.count()))
                completion(count)
            }
        }
        
        HKHealthStore().execute(query)
    }
    
    static func getDistanceWalkingRunning(collectionIn startDate: Date,  completion: @escaping (_ distance: Double) -> Void) {
        
        let startOfDay = Calendar.current.startOfDay(for: startDate)
        let endOfDay = startOfDay.addingTimeInterval((60*60*24)-1)
        
        var dateComponent = DateComponents()
        dateComponent.day = 1
        
        let collectionQuery = HKStatisticsCollectionQuery(
            quantityType: HKQuantityType.distanceWalkingRunning,
            quantitySamplePredicate:  HKQuery.predicateForSamples(withStart: startOfDay, end: endOfDay, options: HKQueryOptions()),
            options: [HKStatisticsOptions.separateBySource, HKStatisticsOptions.cumulativeSum],
            anchorDate: startOfDay,
            intervalComponents: dateComponent
        )
        
        collectionQuery.initialResultsHandler = { query, result, error in
            
            guard let result = result, error == nil else {
                return completion(0)
            }
            DispatchQueue.main.async {
                let sorted = result.statistics().sorted(by: {
                    $0.sumQuantity()!.doubleValue(for: HKUnit.meter()) > $1.sumQuantity()!.doubleValue(for: HKUnit.meter())
                })
                
                guard let first = sorted.first else { return completion(0) }
                
                let meter = floor(first.sumQuantity()!.doubleValue(for: HKUnit.meter()))
                completion(meter)
            }
        }
        
        HKHealthStore().execute(collectionQuery)
    }
    
}

extension UserActivityService {
    
    static func getActivity(collectionIn startDate: Date, completion: @escaping (_ steps: Int, _ distance: Double) -> Void) {
        
        var steps: Int = 0
        var distance: Double = 0.0
        
        let dispatchGroup = DispatchGroup()
        let dispatchQueue = DispatchQueue(label: "\(Bundle.main.bundleIdentifier!).\(#function).\(startDate)")
        
        dispatchGroup.enter()
        dispatchQueue.async(group: dispatchGroup) {
            getSteps(collectionIn: startDate) { _steps in
                steps = _steps
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.enter()
        dispatchQueue.async(group: dispatchGroup) {
            getDistanceWalkingRunning(collectionIn: startDate) { _distance in
                distance = _distance
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            completion(steps, distance)
        }
    }
    
}
