
import Foundation
import RealmSwift

struct UserActivityRealmService {
    
    static let realm: Realm = try! Realm()
    
    static func userStep(in date: Date) -> UserStep? {
        
        let dateStr = UserActivityConfig.dateStr(date: date)
        return realm.object(ofType: UserStep.self, forPrimaryKey: dateStr)
    }
    
    static func userStep(create steps: Int, in date: Date, completion: () -> Void) {
        
        let dateStr = UserActivityConfig.dateStr(date: date)
        
        let userStep = UserStep()
        userStep.dateStr = dateStr
        userStep.steps = steps
        
        try! realm.write {
            realm.add(userStep, update: true)
        }
        completion()
    }
}
